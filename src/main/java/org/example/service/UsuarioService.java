package org.example.service;

import org.example.entity.Usuario;
import org.example.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioService {
        @Autowired
        private UserRepository repository;

        public Usuario saveUsuario(Usuario usuario){
            return repository.save(usuario);
        }
        public List<Usuario> saveUsuarios(List<Usuario> usuarios){
            return repository.saveAll(usuarios);
        }
        public List<Usuario> getUsuarios(){
            return repository.findAll();
        }
        public Usuario getUsuarioById(int id){
            return repository.findById(id).orElse(null);
        }
        public Usuario getUsuarioByName(String nome){
            return repository.findByName(nome);
        }
        public Usuario getUsuarioByEmail(String email){
        return repository.findByEmail(email);
        }
        public Usuario getUsuarioBySenha(String senha){
        return repository.findBySenha(senha);
        }


        public String deleteUsuario (int id){
            repository.deleteById(id);
            return "Usuario removido"+ id;
        }
        public Usuario updateUsuario(Usuario usuario){
            Usuario existingUsuario=repository.findById(usuario.getId()).orElse(null);
           existingUsuario.setNome(usuario.getNome());
           existingUsuario.setTelefone(usuario.getTelefone());
            existingUsuario.setSenha(usuario.getSenha());
            existingUsuario.setEmail(usuario.getEmail());
            return repository.save(existingUsuario);
        }

}
