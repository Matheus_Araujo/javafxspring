package org.example.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.example.Main;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;


public class LoginWindowController {

    @FXML
    private Button cancelButton;
    @FXML
    private Label loginError;
    @FXML
    private Button loginButton;
    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Button cadastroButton;



    public  void  loginButtonOnAction (ActionEvent event) {

        if(!usernameField.getText().isBlank() && !passwordField.getText().isBlank()){
            loginError.setText(" Voce tentou login");



        }else {
            loginError.setText(" Insira o usuario e a Senha");

        }

    }

    public void cancelButtonOnAction(ActionEvent event){
        Stage stage = (Stage)  cancelButton.getScene().getWindow();
        stage.close();

    }
    public void cadastroButtonOnAction (ActionEvent event){
        Stage stage = (Stage)  cadastroButton.getScene().getWindow();
        stage.setTitle("se joga");
        secWindowController();

    }

    public void secWindowController()   {
    try{
        Parent root = FXMLLoader.load(this.getClass().getResource("/fxml/usuarioWindow.fxml"));
        Stage cadastroStage = new Stage();
        cadastroStage.setScene(new Scene(root, 893, 500));
        cadastroStage.setMinWidth(893);
        cadastroStage.setMinHeight(500);
        cadastroStage.show();
    }
    catch (Exception e){
        e.printStackTrace();
        e.getCause();
    }
    }
}
