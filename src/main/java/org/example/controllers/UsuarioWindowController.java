package org.example.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import org.example.entity.Usuario;
import org.example.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


public class UsuarioWindowController {




    @FXML
    private Button closeButton ;




    public void closeButtonOnAction(ActionEvent event){
        Stage stage = (Stage)  closeButton.getScene().getWindow();
        stage.close();

    }
}
