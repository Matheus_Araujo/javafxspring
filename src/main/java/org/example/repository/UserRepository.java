package org.example.repository;

import org.example.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Usuario,Integer> {

    Usuario findByName(String nome);

    Usuario findByEmail(String email);

    Usuario findBySenha(String senha);
}

